package com.example.mani.androidfirstapplication;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by 583583 on 04-07-2016.
 */
public class ListAdapterDaily extends RecyclerView.Adapter<ListAdapterDaily.ViewHolder> {

    Context localContext;
    ArrayList<String> dailyTempMinListLocal, dailyTempMaxListLocal, dailySummaryListLocal;
    TimeAndDayList objTimeAndDayListLocal = new TimeAndDayList();

    public ListAdapterDaily(Context applicationContext, TimeAndDayList objTimeAndDayList,
                            ArrayList<String> dailyTempMinList, ArrayList<String> dailyTempMaxList,
                            ArrayList<String> dailySummaryList) {
        localContext = applicationContext;
        objTimeAndDayListLocal = objTimeAndDayList;
        dailyTempMinListLocal = dailyTempMinList;
        dailyTempMaxListLocal = dailyTempMaxList;
        dailySummaryListLocal = dailySummaryList;

    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // create a new view
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.rowlayoutdaily, parent, false);
        // set the view's size, margins, paddings and layout parameters
        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

        holder.timeHeader.setText(objTimeAndDayListLocal.timeOfCurrentDayList.get(position));
        holder.tempMinHeader.setText(dailyTempMinListLocal.get(position));
        holder.tempMaxHeader.setText(dailyTempMaxListLocal.get(position));
        holder.summaryHeader.setText(dailySummaryListLocal.get(position));


    }

    @Override
    public int getItemCount() {

        return objTimeAndDayListLocal.timeOfCurrentDayList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case
        public TextView timeHeader, tempMinHeader, tempMaxHeader, summaryHeader;

        public ViewHolder(View v) {
            super(v);
            timeHeader = (TextView) v.findViewById(R.id.firstLine);
            tempMinHeader = (TextView) v.findViewById(R.id.secondLine);
            tempMaxHeader = (TextView) v.findViewById(R.id.thirdLine);
            summaryHeader = (TextView) v.findViewById(R.id.fourthLine);
        }


    }
}
