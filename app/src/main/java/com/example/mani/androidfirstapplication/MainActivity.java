package com.example.mani.androidfirstapplication;

import android.app.DownloadManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.support.v7.widget.RecyclerView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.support.v7.widget.LinearLayoutManager;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;;

import android.app.ProgressDialog;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;


import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;
public class MainActivity extends AppCompatActivity {


    GPSTracker gps;
    Button buttonClick;
    TextView responseTextView;
    String url="https://api.forecast.io/forecast/b3773c872bd211754afd57928f0ec9df/37.8267,-122.423";
    TextView dayTextView, summaryTextView, temperatureTextView;
    RelativeLayout appRelativeLayout;
    ImageView weatherIcon;
    private RecyclerView recyclerView, recyclerViewDaily;
    private ProgressDialog pDialog;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        appRelativeLayout = (RelativeLayout) findViewById(R.id.weatherAppBackground);
        dayTextView = (TextView) findViewById(R.id.time);
        summaryTextView = (TextView) findViewById(R.id.summary);
        temperatureTextView = (TextView) findViewById(R.id.temperature);
        recyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        recyclerViewDaily = (RecyclerView) findViewById(R.id.recycler_view_daily);
        weatherIcon = (ImageView) findViewById(R.id.icon);

        RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());

        pDialog = new ProgressDialog(this);
        // Showing progress dialog before making http request
        pDialog.setMessage("Loading...");
        pDialog.show();

        // create class object
        gps = new GPSTracker(MainActivity.this);
        // check if GPS enabled
        if (gps.canGetLocation()) {
            Log.d("","JSONObject_Onrequest") ;
            JsonObjectRequest jsObjRequest = new JsonObjectRequest
                    (Request.Method.GET, url, null, new Response.Listener<JSONObject>() {

                        @Override
                        public void onResponse(JSONObject response) {

                            Log.d("","JSONObject_request"+response) ;

                            String currentWeather = response.optString("currently");
                            String hourlyWeather = response.optString("hourly");
                            String dailyWeather = response.optString("daily");
                            currentlyWeather(currentWeather);
                            hourlyWeather(hourlyWeather);
                            dailyWeather(dailyWeather);
                            pDialog.dismiss();

                        }
                    }, new Response.ErrorListener() {

                        @Override
                        public void onErrorResponse(VolleyError error) {
                            // TODO Auto-generated method stub
                            Toast.makeText(MainActivity.this,error.getMessage(),Toast.LENGTH_LONG).show();
                        }
                    });
            requestQueue.add(jsObjRequest);


        } else {
            // can't get location
            // GPS or Network is not enabled
            // Ask user to enable GPS/network in settings
            gps.showSettingsAlert();
        }
    }

    private void dailyWeather(String dailyWeather) {

        JSONObject hourlyWeatherJSONObject = null;
        String hourlyData;
        ArrayList<String> dailyTimeList = new ArrayList<String>();
        ArrayList<String> dailySummaryList = new ArrayList<String>();
        ArrayList<String> dailyTemperatureList = new ArrayList<String>();
        ArrayList<String> dailyApparentTemperatureList = new ArrayList<String>();
        ArrayList<String> dailyTempMinList = new ArrayList<String>();
        ArrayList<String> dailyTempMaxList = new ArrayList<String>();
        ArrayList<String> dailyWindSpeedList = new ArrayList<String>();
        ArrayList<String> dailyWindBearingList = new ArrayList<String>();
        ArrayList<String> dailyVisibilityList = new ArrayList<String>();
        ArrayList<String> dailyCloudCoverList = new ArrayList<String>();
        ArrayList<String> dailyPressureList = new ArrayList<String>();
        ArrayList<String> dailyOzoneList = new ArrayList<String>();
        TimeAndDayList objTimeAndDayList = new TimeAndDayList();

        try {
            hourlyWeatherJSONObject = new JSONObject(dailyWeather);
            hourlyData = hourlyWeatherJSONObject.optString("data");
            JSONArray jsonArrayHourly = new JSONArray(hourlyData);
            for (int i = 0; i < jsonArrayHourly.length(); i++) {
                JSONObject localJsonObject = (JSONObject) jsonArrayHourly.opt(i);
                dailyTimeList.add(localJsonObject.optString("time"));
                dailySummaryList.add(localJsonObject.optString("summary"));
                dailyTemperatureList.add(localJsonObject.optString("temperature"));
                dailyApparentTemperatureList.add(localJsonObject.optString("apparentTemperature"));
                dailyTempMinList.add(localJsonObject.optString("temperatureMin"));
                dailyTempMaxList.add(localJsonObject.optString("temperatureMax"));
                dailyWindSpeedList.add(localJsonObject.optString("windSpeed"));
                dailyWindBearingList.add(localJsonObject.optString("windBearing"));
                dailyVisibilityList.add(localJsonObject.optString("visibility"));
                dailyCloudCoverList.add(localJsonObject.optString("cloudCover"));
                dailyPressureList.add(localJsonObject.optString("pressure"));
                dailyOzoneList.add(localJsonObject.optString("ozone"));
            }

            objTimeAndDayList = timeConversionHourly(dailyTimeList);
            ListAdapterDaily objListAdapterDaily = new ListAdapterDaily(getApplicationContext(), objTimeAndDayList, dailyTempMinList, dailyTempMaxList,
                    dailySummaryList);
            // specify an adapter (see also next example)
            recyclerViewDaily.setAdapter(objListAdapterDaily);

            LinearLayoutManager mLayoutManagerDaily = new LinearLayoutManager(this);
            mLayoutManagerDaily.setOrientation(LinearLayoutManager.VERTICAL);
            recyclerViewDaily.setLayoutManager(mLayoutManagerDaily);


        } catch (JSONException e) {
            e.printStackTrace();
        }
    }


    private void hourlyWeather(String hourlyWeather) {
        JSONObject hourlyWeatherJSONObject = null;
        String hourlyData;
        ArrayList<String> hourlyTimeList = new ArrayList<String>();
        ArrayList<String> hourlySummaryList = new ArrayList<String>();
        ArrayList<String> hourlyTemperatureList = new ArrayList<String>();
        ArrayList<String> hourlyApparentTemperatureList = new ArrayList<String>();
        ArrayList<String> hourlyDewPointList = new ArrayList<String>();
        ArrayList<String> hourlyHumidityList = new ArrayList<String>();
        ArrayList<String> hourlyWindSpeedList = new ArrayList<String>();
        ArrayList<String> hourlyWindBearingList = new ArrayList<String>();
        ArrayList<String> hourlyVisibilityList = new ArrayList<String>();
        ArrayList<String> hourlyCloudCoverList = new ArrayList<String>();
        ArrayList<String> hourlyPressureList = new ArrayList<String>();
        ArrayList<String> hourlyOzoneList = new ArrayList<String>();
        TimeAndDayList objTimeAndDayList = new TimeAndDayList();
        try {
            hourlyWeatherJSONObject = new JSONObject(hourlyWeather);
            hourlyData = hourlyWeatherJSONObject.optString("data");
            JSONArray jsonArrayHourly = new JSONArray(hourlyData);
            for (int i = 0; i < jsonArrayHourly.length(); i++) {
                JSONObject localJsonObject = (JSONObject) jsonArrayHourly.opt(i);
                hourlyTimeList.add(localJsonObject.optString("time"));
                hourlySummaryList.add(localJsonObject.optString("summary"));
                hourlyTemperatureList.add(localJsonObject.optString("temperature"));
                hourlyApparentTemperatureList.add(localJsonObject.optString("apparentTemperature"));
                hourlyDewPointList.add(localJsonObject.optString("dewPoint"));
                hourlyHumidityList.add(localJsonObject.optString("humidity"));
                hourlyWindSpeedList.add(localJsonObject.optString("windSpeed"));
                hourlyWindBearingList.add(localJsonObject.optString("windBearing"));
                hourlyVisibilityList.add(localJsonObject.optString("visibility"));
                hourlyCloudCoverList.add(localJsonObject.optString("cloudCover"));
                hourlyPressureList.add(localJsonObject.optString("pressure"));
                hourlyOzoneList.add(localJsonObject.optString("ozone"));

            }
            objTimeAndDayList = timeConversionHourly(hourlyTimeList);
            // use a linear layout manager
            ListAdapter objListAdapter = new ListAdapter(getApplicationContext(), objTimeAndDayList, hourlySummaryList, hourlyTemperatureList);

            // specify an adapter (see also next example)
            recyclerView.setAdapter(objListAdapter);

            LinearLayoutManager mLayoutManager = new LinearLayoutManager(this);
            mLayoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);
            recyclerView.setLayoutManager(mLayoutManager);

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void currentlyWeather(String currentWeather) {
        long timeOfCurrentDay = 0;
        JSONObject currentWeatherJSONObject = null;
        try {
            currentWeatherJSONObject = new JSONObject(currentWeather);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        String CW_Summary = currentWeatherJSONObject.optString("summary");
        timeOfCurrentDay = Long.parseLong(currentWeatherJSONObject.optString("time"));
        String CW_Temperature = currentWeatherJSONObject.optString("temperature");
        String CW_ApparentTemperature = currentWeatherJSONObject.optString("apparentTemperature");
        String CW_DewPoint = currentWeatherJSONObject.optString("dewPoint");
        String CW_Humidity = currentWeatherJSONObject.optString("humidity");
        String CW_WindSpeed = currentWeatherJSONObject.optString("windSpeed");
        String CW_WindBearing = currentWeatherJSONObject.optString("windBearing");
        String CW_Visibility = currentWeatherJSONObject.optString("visibility");
        String CW_CloudCover = currentWeatherJSONObject.optString("cloudCover");
        String CW_Pressure = currentWeatherJSONObject.optString("pressure");
        String CW_Ozone = currentWeatherJSONObject.optString("ozone");

        dayTextView.setText("Today");
        summaryTextView.setText("" + CW_Summary);
        temperatureTextView.setText(CW_Temperature);

        if (CW_Summary.equalsIgnoreCase("Light Rain")) {
            weatherIcon.setImageResource(R.mipmap.rain);
        } else if (CW_Summary.equalsIgnoreCase("Rain")) {
            weatherIcon.setImageResource(R.mipmap.heavyrain);
        } else {
            weatherIcon.setImageResource(R.mipmap.sun);
        }

    }


    private TimeAndDayList timeConversionHourly(ArrayList<String> timeOfCurrentDay) {

        long timeOfCurrentDayLong;
        TimeAndDayList objTimeAndDayList = new TimeAndDayList();
        Calendar cal = Calendar.getInstance();
        TimeZone tz = cal.getTimeZone();

        /* debug: is it local time? */
        /* date formatter in local timezone */
        SimpleDateFormat sdfHH = new SimpleDateFormat("hh a");
        SimpleDateFormat sdfDay = new SimpleDateFormat("EEEE");


        /* print your timestamp and double check it's the date you expect */
        Calendar calendar = Calendar.getInstance(Locale.getDefault());


        for (int i = 0; i < timeOfCurrentDay.size(); i++) {
            timeOfCurrentDayLong = Long.parseLong(timeOfCurrentDay.get(i));
            calendar.setTimeInMillis(timeOfCurrentDayLong * 1000);
            String localTime = sdfHH.format(new Date(calendar.getTimeInMillis())); // I assume your timestamp is in seconds and you're converting to milliseconds?
            objTimeAndDayList.timeOfCurrentDayTimeList.add(localTime);
            String localDay = sdfDay.format(new Date(calendar.getTimeInMillis())); // I assume your timestamp is in seconds and you're converting to milliseconds?
            objTimeAndDayList.timeOfCurrentDayList.add(localDay);
        }
        return objTimeAndDayList;
    }

}
