package com.example.mani.androidfirstapplication;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by 583583 on 04-07-2016.
 */
public class ListAdapter extends RecyclerView.Adapter<ListAdapter.ViewHolder> {

    Context localContext;
    ArrayList<String> hourlyTemperatureListLocal, hourlySummaryListLocal;
    TimeAndDayList timeAndDayListLocal;

    public ListAdapter(Context applicationContext, TimeAndDayList timeAndDayList,
                       ArrayList<String> hourlySummaryList,
                       ArrayList<String> hourlyTemperatureList) {
        localContext = applicationContext;
        timeAndDayListLocal = timeAndDayList;
        hourlySummaryListLocal = hourlySummaryList;
        hourlyTemperatureListLocal = hourlyTemperatureList;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // create a new view
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.rowlayout, parent, false);
        // set the view's size, margins, paddings and layout parameters
        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

        holder.tempHeader.setText(hourlyTemperatureListLocal.get(position));
        holder.summaryHeader.setText(hourlySummaryListLocal.get(position));
        holder.timeHeader.setText(timeAndDayListLocal.timeOfCurrentDayTimeList.get(position));

    }

    @Override
    public int getItemCount() {

        return hourlyTemperatureListLocal.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case
        public TextView tempHeader, summaryHeader, timeHeader;

        public ViewHolder(View v) {
            super(v);
            tempHeader = (TextView) v.findViewById(R.id.firstLine);
            summaryHeader = (TextView) v.findViewById(R.id.secondLine);
            timeHeader = (TextView) v.findViewById(R.id.timeLine);
        }


    }
}
